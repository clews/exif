from __future__ import unicode_literals

from __future__ import unicode_literals

__author__ = 'Roberto Rosario'
__build__ = 0x010000
__copyright__ = 'Copyright 2015 Roberto Rosario'
__license__ = 'MIT'
__title__ = 'mayan-exif'
__version__ = '1.0.0'

default_app_config = 'exif.apps.EXIFApp'
